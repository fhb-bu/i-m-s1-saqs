from PyQt4 import QtGui, QtCore
import sqlite3
import Controller

class StationDetails(QtGui.QWidget):

    actualValueError = QtCore.pyqtSignal(str)## Fehlersignal wenn actual keine natÃ¼rliche Zahl
    valuesChangedSignal = QtCore.pyqtSignal()## DatensÃ¤tze wurden erfolgreich gespeichert

    def __init__(self):
        super(StationDetails, self).__init__()
        self.initUI()

    """
        Initialize Widget.
    """
    def initUI(self):

        #set up Lines for Station Information
        self.stationName = QtGui.QLineEdit(readOnly = True)
        stationIdLabel = QtGui.QLabel('Station Name')
        self.date = QtGui.QLineEdit()
        dateLabel = QtGui.QLabel('Date')
        self.target = QtGui.QLineEdit(readOnly = True)
        targetLabel = QtGui.QLabel('Target')
        self.actual = QtGui.QLineEdit()
        actualLabel = QtGui.QLabel('Actual')
        self.variance = QtGui.QLineEdit(readOnly = True)
        varianceLabel = QtGui.QLabel('Variance')
        self.actual.textChanged.connect(self.displayVariance)
        self.statusVariance = 0

        #set up button for saving changes
        self.saveButton = QtGui.QPushButton('SAVE', self)
        self.saveButton.clicked.connect(self.saveValues)

        #set up station List
        self.stationList = QtGui.QListWidget()
        self.stationList.currentItemChanged.connect(self.get_station_values)
        stationListLabel = QtGui.QLabel('Station List')

        #set up Layout
        grid = QtGui.QGridLayout()
        grid.setSpacing(6)

        #add station list and edit fields to layout
        grid.addWidget(stationListLabel, 1, 0)
        grid.addWidget(self.stationList, 1, 1, 5, 1)

        grid.addWidget(stationIdLabel, 1, 2)
        grid.addWidget(self.stationName, 1, 3)
        grid.addWidget(dateLabel, 2, 2)
        grid.addWidget(self.date, 2, 3)
        grid.addWidget(targetLabel, 3, 2)
        grid.addWidget(self.target, 3, 3)
        grid.addWidget(actualLabel, 4, 2)
        grid.addWidget(self.actual, 4, 3)
        grid.addWidget(varianceLabel, 5, 2)
        grid.addWidget(self.variance, 5, 3)
        grid.addWidget(self.saveButton, 6, 3)

        #set layout and get initial station list + values
        self.setLayout(grid)
        self.get_all_stations()
        self.show()

    """
        Sets edit fields. Also changes the color of the edit fields for the 'Variance' field and not editable fields.
        @given: list-values and station details for the edit fields.
    """
    def refresh_values(self, list):
        #If Values are given
        if len(list) == 4:
            self.stationName.setText(str(list[0]))
            self.stationName.setStyleSheet("QLineEdit{background: gainsboro;}")#not editable field
            self.date.setText(str(list[1]))
            self.target.setText(str(list[2]))
            self.target.setStyleSheet("QLineEdit{background: gainsboro;}")#not editable
            self.actual.setText(str(list[3]))
            self.displayVariance()
        #if values are not given
        elif len(list) == 2:
            self.stationName.setText(str(list[0]))
            self.stationName.setStyleSheet("QLineEdit{background: gainsboro;}")#not editable
            self.date.clear()
            self.target.setText(str(list[1]))
            self.target.setStyleSheet("QLineEdit{background: gainsboro;}")#not editable
            self.actual.clear()
            self.variance.clear()
            self.variance.setStyleSheet("QLineEdit{background: gainsboro;}")#not editable
        return True

    """
        Sets 'variance' field
    """
    def displayVariance(self):
        if self.actual.text():
            variance = Controller.calculateVariance(int(self.target.text().strip()), int(self.actual.text().strip()))

            self.variance.setText(str(variance))
            if variance <= 90:
                self.variance.setStyleSheet("QLineEdit{background: lightcoral;}")#variance 90% or less
                self.statusVariance = 0
            elif variance >= 105:
                self.variance.setStyleSheet("QLineEdit{background: darkseagreen;}")#variance 105% or more
                self.statusVariance = 1
            else:
                self.variance.setStyleSheet("QLineEdit{background: gainsboro;}")#variance between 90 and 105%
                self.statusVariance = 2

    """
        Clears station list and (re)fills it.
    """
    def refresh_list(self, list):
        self.stationList.clear()
        for row in list:
            self.stationList.addItem(row[0])

    """
        Fetch the name of all stations
    """
    def get_all_stations(self):
        station_list = Controller.get_all_stations()
        self.refresh_list(station_list)
        return station_list

    """
        Fetch values that belong to a station 'station_name' and fill the edit fields.
    """
    def get_station_values(self,station_name):
        value_response = Controller.getStationDetails(station_name.text())
        self.refresh_values(value_response)

        return value_response

    """
        Save Values in the edit fields and save them. The reference station is the one named in 'station name'.
    """
    def saveValues(self):
        if Controller.saveValues(self.stationName.text(), self.date.text().strip(), self.actual.text().strip(), self.statusVariance):
            self.valuesChangedSignal.emit()
            QtGui.QMessageBox.information(QtGui.QMessageBox(),"Information","Änderungen wurden gespeichert")
        else:
            self.actualValueError.emit("There was an error.")
