import sqlite3
import sys
from PyQt4 import QtGui

class Stations(QtGui.QWidget):
    def __init__(self,name,status):
        QtGui.QWidget.__init__(self)
        pixmap = QtGui.QPixmap('icon.jpg')
        vBoxlayout	= QtGui.QVBoxLayout()

        self.iconLabel = QtGui.QLabel()
        self.nameLabel = QtGui.QLabel(name)
        self.nameLabel.setMaximumHeight(30)
        self.nameLabel.setMaximumWidth(100)

        try:
            if int(status) == -1:
                self.nameLabel.setStyleSheet("QLabel{background: gainsboro;}")
                pixmap = QtGui.QPixmap('icon_g.jpg')
            elif int(status) == 0:
                self.nameLabel.setStyleSheet("QLabel{background: lightcoral;}")
            elif int(status) == 1:
                self.nameLabel.setStyleSheet("QLabel{background: darkseagreen;}")
            elif int(status) == 2:
                self.nameLabel.setStyleSheet("QLabel{background: gainsboro;}")
        except:
            print("Konnte Status nicht interpretieren")

        self.iconLabel.setPixmap(pixmap)
        vBoxlayout.addWidget(self.iconLabel)
        vBoxlayout.addWidget(self.nameLabel)
        self.setLayout(vBoxlayout)


class StationsGUI(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.datenBank = "database.db"
        self.gridLayout = QtGui.QGridLayout()

        self.items = []
        self.erstelleLayout()
        self.updateLayout()

    def updateLayout(self):
        for i in reversed(range(self.gridLayout.count())):
            self.gridLayout.itemAt(i).widget().setParent(None)
        self.erstelleLayout()

    def erstelleLayout(self):
        stationen = self.holeStationsStatus()
        x = 0
        y = 0
        for eintrag in stationen:
            station = Stations(eintrag[0],eintrag[1])
            self.items.append(station)
            self.gridLayout.addWidget(station,x,y)
            if y < 3:
                y+=1
            else:
                y=0
                x+=1
        self.setLayout(self.gridLayout)




    def holeStationen(self):
        try:
            connection = sqlite3.connect("database.db")
            cursor = connection.cursor()
            cursor.execute("SELECT id FROM stations;")
            return self.tupleToList(cursor.fetchall())
        except:
            print("Fehler beim Holen der Stationen")

    def holeStationsStatus(self):
        stationen = []
        try:
            for id in self.holeStationen():
                cursor = sqlite3.connect(self.datenBank).cursor()
                cursor.execute("SELECT name,status FROM stations WHERE id={0};".format(id[0]))
                station = self.tupleToList(cursor.fetchall())
                if station != []:
                    stationen.append(station[0])
        except:
            print("Fehler beim Holen der Stationsdaten")
        return stationen

    def tupleToList(self,tuple):
        tmpListe = []
        for zeile in tuple:
            if zeile != None:
                zeilenListe = []
                for i in range(0,len(zeile)):
                    zeilenListe.append(zeile[i])
                tmpListe.append(zeilenListe)
        return tmpListe

if __name__ == "__main__":
    app 	= QtGui.QApplication(sys.argv)
    vBoxlayout	= QtGui.QVBoxLayout()
    vBoxlayout.addWidget(StationsGUI())
    mainGUI = QtGui.QWidget()
    mainGUI.setLayout(vBoxlayout)
    mainGUI.show()

    sys.exit(app.exec_())
