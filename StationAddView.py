import sqlite3
from PyQt4 import QtGui, QtCore

class addStationWidget(QtGui.QWidget):

    errorSignal = QtCore.pyqtSignal(str)## Fehlersignal
    stationAddedSignal = QtCore.pyqtSignal()## erfolgreiches Hinzufügen einer Station


    def __init__(self, parent = None):
        super(QtGui.QWidget, self).__init__()
        self.initUI()


    ## Erstellt das Layout und die einzelnen GUI-Elemente dieser View
    def initUI(self):

        self.errorSignal.connect(self.errorSignalSlot)
        editLayout = QtGui.QGridLayout()
        mainLayout = QtGui.QVBoxLayout(self)
        buttonLayout = QtGui.QHBoxLayout()
        self.addBtn = QtGui.QPushButton("ADD",self)
        self.addBtn.clicked.connect(self.addStation)

        self.nameLabel = QtGui.QLabel("Name")
        self.nameEdit = QtGui.QLineEdit()

        self.targetLabel = QtGui.QLabel("Target Value")
        self.targetEdit = QtGui.QLineEdit()

        editLayout.addWidget(self.nameLabel, 0, 0)
        editLayout.addWidget(self.nameEdit, 0, 1)
        editLayout.addWidget(self.targetLabel, 1, 0)
        editLayout.addWidget(self.targetEdit, 1, 1)
        buttonLayout.addWidget(self.addBtn)
        buttonLayout.setAlignment(QtCore.Qt.AlignHCenter)
        mainLayout.addLayout(editLayout)
        mainLayout.addLayout(buttonLayout)


    ## fügt Station hinzu, prüft die Eingabe und sendet ein Änderungssignal
    def addStation(self):
        name = self.nameEdit.text().strip()
        targetValue = self.targetEdit.text().strip()
        try:
            int(targetValue)
        except ValueError:
            self.errorSignal.emit("Fehlerhafte Eingabe für den Target Value")
            return
        if name != "":
            conn = sqlite3.connect("database.db")
            cursor = conn.cursor()
            station_list_result = cursor.execute("SELECT * FROM stations WHERE name = ?",[name]).fetchall()
            if station_list_result == []:
                cursor.execute("INSERT INTO stations (name,target_value) Values(?,?)",(name,targetValue))
                conn.commit()
                self.stationAddedSignal.emit()
                QtGui.QMessageBox.information(QtGui.QMessageBox(),"Information","Station wurde hinzugefügt")
            else:
                self.errorSignal.emit("Die Station " + name + " existiert bereits")
        else:
            self.errorSignal.emit("Fehlerhafte Eingabe des Namens")
        conn.close()


    @QtCore.pyqtSlot(str)
    def errorSignalSlot(self,msg):
        QtGui.QMessageBox.critical(QtGui.QMessageBox(),"Fehler",msg)


