
import sys
from PyQt4 import QtGui


from StationAddView import addStationWidget
from StationDetailsView import StationDetails
from StationCompleteView import StationsGUI

"""
    Set up window including the widgets 'Station Details' and 'Add Station'.
"""
class Window(QtGui.QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 640, 480)
        self.setWindowTitle('Ice Cream Concentration Control')
        self.setWindowIcon(QtGui.QIcon('web.png'))
        editLayout = QtGui.QGridLayout(self)
        completeWidget = StationsGUI()
        detailsWidget = StationDetails()
        detailsWidget.valuesChangedSignal.connect(completeWidget.updateLayout)
        addStation = addStationWidget()
        addStation.stationAddedSignal.connect(detailsWidget.get_all_stations)
        addStation.stationAddedSignal.connect(completeWidget.updateLayout)
        tabs = QtGui.QTabWidget()
        tabs.addTab(completeWidget,'Overview')
        tabs.addTab(detailsWidget,"Station Details")
        tabs.addTab(addStation,"Add Station")
        editLayout.addWidget(tabs,0,0)
        self.show()


application = QtGui.QApplication(sys.argv)
activeView = Window()
sys.exit(application.exec_())
