import sqlite3

DB = 'database.db'

#returns variance in percent
def calculateVariance(target, actual):
    return round(actual / target * 100, 2)

#returns array of station names
def get_all_stations():
    con = sqlite3.connect(DB)
    cur = con.cursor()
    cur.execute("SELECT name FROM stations")
    r = cur.fetchall()
    con.close()

    return r

#get values of station by name, returns values as array
def getStationDetails(station_name):
    con = sqlite3.connect(DB)
    cur = con.cursor()

    station = cur.execute("SELECT id, name, target_value FROM stations WHERE name = ?", [station_name]).fetchone()
    values = cur.execute("SELECT date, value FROM vals WHERE vals.sid = ?", [station[0]]).fetchone()

    if station and values:
        r = [station[1], values[0], station[2], values[1]]
    elif station:
        r = [station[1], station[2]]

    con.close()

    return r

"""
    Save Values in the edit fields and save them. The reference station is the one named in 'station name'.
"""
def saveValues(station_name, date, actual, variance_status):
    con = sqlite3.connect(DB)
    cur = con.cursor()
    station_id = cur.execute("SELECT id FROM stations WHERE name = ?", [station_name]).fetchone()[0]

    try:
        int(actual)
    except ValueError:
        return False

    if date and actual:
        if cur.execute("SELECT date, value FROM vals WHERE sid = ?", [station_id]).fetchone() is not None:
            cur.execute("UPDATE vals SET date = ?, value = ? WHERE sid = ?", [date, actual, station_id])
        else:
            cur.execute("INSERT INTO vals (sid, date, value) Values(? ,? ,?)", (station_id, date, actual))

        cur.execute("UPDATE stations SET status=? WHERE id= ?", (variance_status, station_id))
        con.commit()
    else:
        return False

    con.close()

    return True
